# wisdomgrid

#### 介绍

智慧方物联网平台

集合设备控制、管理的云原生平台

具有产品：
1.环境监测
2.小夜灯
3.彩灯

官网：
wisdomgrid.cn

### 一、客户端

 **1.WebApp** 

测试版本：

http://devapp.onelink.wisdomgrid.cn/

测试

账号：8422725037@qq.com
密码：123456


 **2.管理后台** 


测试版本：

http://devmangeui.onelink.wisdomgrid.cn/



### 二、部分开源代码


 **1.OneLink.Code**  

    .net 基础库封装
    https://gitee.com/wisdomgrid/OneLink.Code

   

### 三、联系作者

微信（qq）：842725037


